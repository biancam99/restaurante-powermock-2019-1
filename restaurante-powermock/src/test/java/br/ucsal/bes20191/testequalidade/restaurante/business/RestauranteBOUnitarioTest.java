package br.ucsal.bes20191.testequalidade.restaurante.business;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import br.ucsal.bes20191.testequalidade.restaurante.domain.Comanda;
import br.ucsal.bes20191.testequalidade.restaurante.domain.Mesa;
import br.ucsal.bes20191.testequalidade.restaurante.enums.SituacaoMesaEnum;
import br.ucsal.bes20191.testequalidade.restaurante.persistence.ComandaDao;
import br.ucsal.bes20191.testequalidade.restaurante.persistence.MesaDao;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ComandaDao.class, MesaDao.class })
public class RestauranteBOUnitarioTest {

	/**
	 * Metodo a ser testado: public static Integer abrirComanda(Integer numeroMesa)
	 * throws RegistroNaoEncontrado, MesaOcupadaException. Verificar se a abertura
	 * de uma comanda para uma mesa livre apresenta sucesso. Lembre-se de verificar
	 * a chamada ao ComandaDao.incluir(comanda).
	 */
	@Before
	public void init() {
	}

	@Test
	public void abrirComandaMesaLivre() throws Exception {
		Integer numeroMesa = 211;
		Mesa mesa = new Mesa(numeroMesa);
		mesa.setSituacao(SituacaoMesaEnum.LIVRE);

		PowerMockito.mockStatic(MesaDao.class);
		PowerMockito.when(MesaDao.obterPorNumero(numeroMesa)).thenReturn(mesa);

		// OU moca toda a classe.
		PowerMockito.mockStatic(ComandaDao.class);

		// OU espiona e, para n�o executar determinado comportamento, faz um doNothing.
		// PowerMockito.spy(ComandaDao.class);
		// PowerMockito.doNothing().when(ComandaDao.class, "incluir",
		// Mockito.any(Comanda.class));

		//Se restauranteBO fosse privado
		// Whitebox.invokeMethod(RestauranteBO.class, "abrirComanda", numeroMesa);
		//Como � public |>
		RestauranteBO.abrirComanda(numeroMesa);

		PowerMockito.verifyStatic(ComandaDao.class);
		ComandaDao.incluir(Mockito.any(Comanda.class));

	}
}
